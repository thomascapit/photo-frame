const proxy = require('http-proxy-middleware');

module.exports = function(app) {
    app.use(proxy('/cheerzapi', {
        changeOrigin: true,
        pathRewrite: {'^/cheerzapi':''},
        target: 'https://live.cheerz.com'
    }));
};