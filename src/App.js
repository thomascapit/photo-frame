import React from 'react';
import * as ReactDOM from 'react-dom'
import axios from 'axios'
import logo from './logo.svg';
import './App.css';

function App() {
    var images = [];
    var texts = [];

  axios.get("/cheerzapi/locations/6NTXQ.json?token=97708656fa84355530cc08df696f16e967e8c921")
      .then((res) => {
          images = res.data.location.customization.Pola.images;
          texts = res.data.location.customization.Pola.texts;
          ReactDOM.render(
              images.map((img, key) => {
                  const imgStyle = {
                      position: 'absolute',
                      top: img.y * img.ratio,
                      left: img.x * img.ratio,
                      width: img.width
                  };
                  return (<img
                      src={img.url}
                      alt=""
                      style={imgStyle}
                  />)
              }),
              document.getElementById("ImgContainer")
          );
          ReactDOM.render(
              texts.map((txt, key) => {
                  const imgStyle = {
                      position: 'absolute',
                      top: txt.y - txt.size,
                      left: 200,
                      width: 850,
                      fontSize: txt.size,
                      textAlign: txt.align,
                      color: txt.color,
                      fontFamily: txt.font
                  };
                  return (<span style={imgStyle}>{txt.text}</span>)
              }),
              document.getElementById("TxtContainer")
          );
      })
      .catch((error) => {
          console.log(error);
      });

  return (
      <div className="App">
          <div id="ImgContainer"></div>
          <div id="TxtContainer"></div>
      </div>
  );
}

export default App;
